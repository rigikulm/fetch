// fetch - prints the content from one (or more) URLs
package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

const httpPrefix = "http://"

func main() {
	// Display a usage message if no URLs were provided
	if len(os.Args) < 2 {
		fmt.Println("usage: fetch <url> [url2 url3...]")
		os.Exit(1)
	}

	// For each provided URL attempt to retrieve the contents
	// http.Get() - opens stream to get the data
	// io.Copy() - copies from the stream to stdout
	for _, url := range os.Args[1:] {
		// Add the 'http://' prefix if it is missing
		if !strings.HasPrefix(url, httpPrefix) {
			url = httpPrefix + url
		}

		resp, err := http.Get(url)
		if err != nil {
			log.Fatal(err)
		}

		_, copyErr := io.Copy(os.Stdout, resp.Body)
		if copyErr != nil {
			log.Fatal(copyErr)
		}
		resp.Body.Close()
	}
}
